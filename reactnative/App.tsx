import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialButtonSuccess from "./MaterialButtonSuccess";
import Icon from "react-native-vector-icons/Feather";
import CupertinoFooter2 from "./CupertinoFooter2";
import CupertinoHeaderWithAddButton from "./CupertinoHeaderWithAddButton";
import MyCarousel from "./MyCarousel";


function Untitled(props) {
  return (
    <View style={styles.container}>
      <MaterialButtonSuccess
        style={styles.materialButtonSuccess}
      ></MaterialButtonSuccess>
      <Text style={styles.acceso}>Acceso</Text>
      <View style={styles.rect}></View>
      <Icon name="alert-circle" style={styles.icon}></Icon>
      <Text style={styles.comienceConUna}>
        Comience con una consulta gratuita sobre su informe de crédito
      </Text>
      <Text style={styles.comienceConUna1}>
        Líder confiable en calificación {"\n"}crediticia
      </Text>
      <CupertinoFooter2 style={styles.cupertinoFooter2}></CupertinoFooter2>
      <CupertinoHeaderWithAddButton
        style={styles.cupertinoHeaderWithAddButton}
      ></CupertinoHeaderWithAddButton>
       <MyCarousel /> 
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  materialButtonSuccess: {
    height: 36,
    width: 275,
    position: "absolute",
    left: 23,
    top: 428,
    backgroundColor: "rgba(102,211,218,1)"
  },
  acceso: {
    top: 473,
    position: "absolute",
    fontFamily: "roboto-700",
    color: "rgba(15,19,171,1)",
    fontSize: 20,
    left: 127
  },
  rect: {},
  icon: {
    top: 69,
    left: 9,
    position: "absolute",
    color: "rgba(128,128,128,1)",
    fontSize: 27
  },
  comienceConUna: {
    top: 371,
    position: "absolute",
    fontFamily: "roboto-700",
    color: "rgba(35,92,62,1)",
    fontSize: 16,
    left: 29,
    width: 261,
    height: 57,
    textAlign: "center"
  },
  comienceConUna1: {
    top: 69,
    position: "absolute",
    fontFamily: "roboto-700",
    color: "rgba(62,90,25,1)",
    fontSize: 16,
    left: 57,
    width: 206,
    height: 68,
    textAlign: "center"
  },
  cupertinoFooter2: {
    height: 49,
    width: 328,
    position: "absolute",
    left: -8,
    top: 519
  },
  cupertinoHeaderWithAddButton: {
    height: 44,
    width: 320,
    position: "absolute",
    left: 0,
    top: 16
  }
});

export default Untitled;
