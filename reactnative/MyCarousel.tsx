import React from 'react';
import { View, Image, Dimensions } from 'react-native';
import Carousel from 'react-native-snap-carousel';

const { width: screenWidth } = Dimensions.get('window');

const MyCarousel = () => {
  const images = [
    { uri: 'https://media.istockphoto.com/id/1394055240/es/foto/feliz-chef-negra-preparando-comida-en-sart%C3%A9n-en-la-cocina-del-restaurante.jpg?s=612x612&w=0&k=20&c=Cgn9lpbLhcYcSCaq3GuXblBVxD4WGVMD-T9NvLTXKfI=' },
    { uri: 'https://media.istockphoto.com/id/1414024629/es/foto/profesor-de-lectura-en-la-universidad.jpg?s=612x612&w=0&k=20&c=IauFRfQxiqpXsBVDeng1OQoDxPJtk7388GXJW-WQ6Tc=' },
    { uri: 'https://media.istockphoto.com/id/1487551249/es/vector/ayuda-psicol%C3%B3gica-vector-ilustraci%C3%B3n-dos-cabezas-humanas-dise%C3%B1o-de-corte-de-papel.jpg?s=612x612&w=0&k=20&c=6UnEvtyweJ8Aw6l7-YMke3jhs6i5omTyfQ9DTe49sOc=' },
  ];

  const renderItem = ({ item, index }) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Image
          source={{ uri: item.uri }}
          style={{ width: screenWidth - 60, height: 200 }}
          resizeMode="cover"
        />
      </View>
    );
  };

  return (
    <Carousel
      data={images}
      renderItem={renderItem}
      sliderWidth={screenWidth}
      itemWidth={screenWidth - 60}
      loop={true}
      autoplay={true}
      autoplayDelay={500}
      autoplayInterval={3000}
    />
  );
};

export default MyCarousel;
