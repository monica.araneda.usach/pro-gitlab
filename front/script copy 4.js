const jsonData = [
  {
      "id": "bahdguy-efx-001-efx",
      "reportGenerated": 1456800680627,
      "creditFileSecurityFreezeFlag": false,
      "reportType": "US_EFX",
      "provider": "EFX",
      "subject": {
          "provider": "EFX",
          "currentName": {
              "lastName": "Guy",
              "firstName": "Bahd",
              "middleName": "N",
              "suffix": "JR"
          },
          "aliases": [
              {
                  "lastName": "Moriarty",
                  "firstName": "James",
                  "middleName": "S",
                  "suffix": ""
              }
          ],
          "currentAddress": {
              "country": {
                  "code": "USA",
                  "name": null
              },
              "line1": "456 London Ave",
              "line2": "",
              "line3": "Nowhere",
              "line4": "ID",
              "line5": "30006",
              "firstReportedDate": 1267419600000,
              "lastReportedDate": 1450328400000,
              "phone": {
                  "countryCode": "1",
                  "areaCode": "555",
                  "exchange": "867",
                  "extension": "1432"
              }
          },
          "previousAddresses": [
              {
                  "country": {
                      "code": "USA",
                      "name": null
                  },
                  "line1": "130 Villain RD",
                  "line2": "",
                  "line3": "Fairburn",
                  "line4": "GA",
                  "line5": "30213",
                  "firstReportedDate": 1009861200000,
                  "lastReportedDate": 1267419600000,
                  "phone": null
              },
              {
                  "country": {
                      "code": "USA",
                      "name": null
                  },
                  "line1": "12400 Thornberry Rd Nw",
                  "line2": "Apt 301",
                  "line3": "Silverdale",
                  "line4": "WA",
                  "line5": "98383",
                  "firstReportedDate": 951886800000,
                  "lastReportedDate": 1271822400000,
                  "phone": {
                      "countryCode": "1",
                      "areaCode": "501",
                      "exchange": "555",
                      "extension": "1212"
                  }
              }
          ],
          "homePhone": {
              "countryCode": "1",
              "areaCode": "555",
              "exchange": "867",
              "extension": "1432"
          },
          "mobilePhone": {
              "countryCode": "1",
              "areaCode": "555",
              "exchange": "867",
              "extension": "1432"
          },
          "nationalIdentifier": "xxxxx 7890",
          "dateOfBirth": 649742400000,
          "dateOfDeath": null,
          "employmentHistory": [
              {
                  "employerName": "ACME",
                  "employeeTitle": "ANALYST OF DIABOLICAL PLANNING",
                  "currentEmployer": true,
                  "ordinal": 1,
                  "dateOfEmployment": 1288584000000,
                  "address": {
                      "country": {
                          "code": "USA",
                          "name": null
                      },
                      "line1": "",
                      "line2": "",
                      "line3": "HBR HGTS",
                      "line4": "OH",
                      "line5": "",
                      "firstReportedDate": null,
                      "lastReportedDate": null,
                      "phone": null
                  }
              },
              {
                  "employerName": "CIRCUS BURGER",
                  "employeeTitle": "CASHIER",
                  "currentEmployer": false,
                  "ordinal": 2,
                  "dateOfEmployment": null,
                  "address": {
                      "country": {
                          "code": "USA",
                          "name": null
                      },
                      "line1": "",
                      "line2": "",
                      "line3": "MDWY",
                      "line4": "OH",
                      "line5": "",
                      "firstReportedDate": null,
                      "lastReportedDate": null,
                      "phone": null
                  }
              }
          ]
      },
      "creditScore": {
          "score": 480,
          "provider": "EFX",
          "scoreRanges": [
              {
                  "low": 280,
                  "high": 559,
                  "name": "Poor"
              },
              {
                  "low": 560,
                  "high": 659,
                  "name": "Fair"
              },
              {
                  "low": 660,
                  "high": 724,
                  "name": "Good"
              },
              {
                  "low": 725,
                  "high": 759,
                  "name": "Very Good"
              },
              {
                  "low": 760,
                  "high": 850,
                  "name": "Excellent"
              }
          ],
          "loanRiskRanges": [
              {
                  "low": 280,
                  "high": 599,
                  "name": "Very High Risk"
              },
              {
                  "low": 600,
                  "high": 660,
                  "name": "High Risk"
              },
              {
                  "low": 661,
                  "high": 715,
                  "name": "Medium Risk"
              },
              {
                  "low": 716,
                  "high": 747,
                  "name": "Low Risk"
              },
              {
                  "low": 748,
                  "high": 850,
                  "name": "Very Low Risk"
              }
          ],
          "scoreReasons": [
              {
                  "code": "04",
                  "description": "",
                  "creditScoreFactorEffect": "HURTING"
              },
              {
                  "code": "34",
                  "description": "",
                  "creditScoreFactorEffect": "HURTING"
              },
              {
                  "code": "48",
                  "description": "",
                  "creditScoreFactorEffect": "HURTING"
              },
              {
                  "code": "94",
                  "description": "",
                  "creditScoreFactorEffect": "HURTING"
              }
          ]
      },
      "revolvingAccounts": {
          "balance": {
              "amount": 659.0,
              "currency": "USD"
          },
          "creditLimit": {
              "amount": 600.0,
              "currency": "USD"
          },
          "available": {
              "amount": -59.0,
              "currency": "USD"
          },
          "monthlyPaymentAmount": {
              "amount": 48.0,
              "currency": "USD"
          },
          "debtToCreditRatio": 110.0,
          "totalAccounts": 2,
          "totalNegativeAccounts": 0,
          "totalAccountsWithBalance": 2
      },
      "mortgageAccounts": {
          "balance": {
              "amount": 248100.0,
              "currency": "USD"
          },
          "creditLimit": {
              "amount": 248100.0,
              "currency": "USD"
          },
          "available": {
              "amount": 0.0,
              "currency": "USD"
          },
          "monthlyPaymentAmount": {
              "amount": 516.0,
              "currency": "USD"
          },
          "debtToCreditRatio": 100.0,
          "totalAccounts": 1,
          "totalNegativeAccounts": 0,
          "totalAccountsWithBalance": 1
      },
      "installmentAccounts": {
          "balance": {
              "amount": 11471.0,
              "currency": "USD"
          },
          "creditLimit": {
              "amount": 15130.0,
              "currency": "USD"
          },
          "available": {
              "amount": 3659.0,
              "currency": "USD"
          },
          "monthlyPaymentAmount": {
              "amount": 329.0,
              "currency": "USD"
          },
          "debtToCreditRatio": 76.0,
          "totalAccounts": 1,
          "totalNegativeAccounts": 0,
          "totalAccountsWithBalance": 1
      },
      "otherAccounts": {
          "balance": {
              "amount": 315.0,
              "currency": "USD"
          },
          "creditLimit": {
              "amount": 124.0,
              "currency": "USD"
          },
          "available": {
              "amount": 439.0,
              "currency": "USD"
          },
          "monthlyPaymentAmount": {
              "amount": 23.0,
              "currency": "USD"
          },
          "debtToCreditRatio": 72.0,
          "totalAccounts": 2,
          "totalNegativeAccounts": 0,
          "totalAccountsWithBalance": 1
      },
      "totalOpenAccounts": {
          "balance": {
              "amount": 260545.0,
              "currency": "USD"
          },
          "creditLimit": {
              "amount": 264269.0,
              "currency": "USD"
          },
          "available": {
              "amount": 3724.0,
              "currency": "USD"
          },
          "monthlyPaymentAmount": {
              "amount": 916.0,
              "currency": "USD"
          },
          "debtToCreditRatio": 99.0,
          "totalAccounts": 6,
          "totalNegativeAccounts": 0,
          "totalAccountsWithBalance": 5
      },
      "lengthOfCreditHistoryMonths": 0,
      "totalNegativeAccounts": 0,
      "averageAccountAgeMonths": 26,
      "oldestAccountOpenDate": 1335844800000,
      "oldestAccountName": "HEIGHTS FINANCE",
      "mostRecentAccountOpenDate": 1446350400000,
      "mostRecentAccountName": "DCUMORTGAGE",
      "totalConsumerStatements": 3,
      "mostRecentInquiryDate": null,
      "mostRecentInquiryName": null,
      "totalPersonalInformation": 9,
      "totalInquires": 6,
      "totalPublicRecords": 5,
      "totalCollections": 2,
      "disputeInformation": {
          "contactName": "Equifax",
          "address": {
              "country": {
                  "code": "USA",
                  "name": null
              },
              "line1": "123 Main St",
              "line2": "",
              "line3": "Nowhere",
              "line4": "ID",
              "line5": "3005",
              "firstReportedDate": null,
              "lastReportedDate": null,
              "phone": null
          },
          "phone": {
              "countryCode": "1",
              "areaCode": "555",
              "exchange": "867",
              "extension": "5309"
          }
      }
  }
]; // Asegúrate de reemplazar esto con tu JSON



// Calcula el valor para el gauge del puntaje de crédito
function updateCreditScoreGauge(creditScore) {
    const gaugeElement = document.querySelector(".gauge .gauge__fill");
    const gaugeCover = document.querySelector(".gauge .gauge__cover");
    const score = creditScore.score;
    const maxScore = 850; // Asumiendo el máximo puntaje posible
    const minScore = 280; // Asumiendo el mínimo puntaje posible
    const scoreFraction = (score - minScore) / (maxScore - minScore);

    gaugeElement.style.transform = `rotate(${scoreFraction / 2}turn)`;

    gaugeCover.textContent = score.toString();

}
// Suponiendo que 'data' es tu JSON parseado
updateCreditScoreGauge(jsonData[0].creditScore);

 

/* 
const gaugeElement = document.querySelector(".gauge");
function setGaugeValue(gauge, value) {
    if (value < 0 || value > 1) {
        return;
    }
    gauge.querySelector(".gauge__fill").style.transform = `rotate(${value / 2}turn)`;
    gauge.querySelector(".gauge__cover").textContent = `${Math.round(value * 100  * 24)}`
}
setGaugeValue(gaugeElement, 0.3); */


function displayBasicInfo(subject) {
    const infoBox = document.createElement('div');
    infoBox.className = 'info-box';
    infoBox.innerHTML = `
        <h3>Información Básica</h3>
        <p>Nombre: ${subject.currentName.firstName} ${subject.currentName.middleName} ${subject.currentName.lastName}</p>
        <p>Dirección: ${subject.currentAddress.line1}, ${subject.currentAddress.line3}, ${subject.currentAddress.line4}, ${subject.currentAddress.line5}</p>
        <p>Teléfono: (${subject.homePhone.areaCode}) ${subject.homePhone.exchange}-${subject.homePhone.extension}</p>
    `;
    document.querySelector('.container').appendChild(infoBox);
}

function displayAccountInfo(revolvingAccounts) {
  const infoBox = document.createElement('div');
  infoBox.className = 'info-box';
  infoBox.innerHTML = `
      <h3>Información Cuenta</h3>
      <p>Balance: ${revolvingAccounts.balance.amount} ${revolvingAccounts.balance.currency}</p>
      <p>Limite de Credito: ${revolvingAccounts.creditLimit.amount} ${revolvingAccounts.creditLimit.currency} </p>
      <p>Disponble: (${revolvingAccounts.available.amount}) ${revolvingAccounts.available.currency}</p>
      <p>Monto de Pago Mensual: ${revolvingAccounts.monthlyPaymentAmount.amount} ${revolvingAccounts.monthlyPaymentAmount.currency}</p>
      <p>Relacion Deuda-Credito: ${revolvingAccounts.debtToCreditRatio} </p>
      <p>Total Cuentas: ${revolvingAccounts.totalAccounts} </p>
      <p>Total Cuentas Negativas: ${revolvingAccounts.totalNegativeAccounts} </p>
      <p>Total Cuentas con Saldo: ${revolvingAccounts.totalAccountsWithBalance} </p>      
  `;

  document.querySelector('.container').appendChild(infoBox);
}


function displayEmploymentHistory(employmentHistory) {
    const employmentSection = document.createElement('div');
    employmentSection.className = 'section';
    employmentSection.innerHTML = '<h3>Historial de Empleo</h3>';
    const row = document.createElement('div');
    row.className = 'row';

    employmentHistory.forEach(job => {
        const column = document.createElement('div');
        column.className = 'column';
        const jobInfo = document.createElement('div');
        jobInfo.className = 'info-box';
        const dateOfEmployment = job.dateOfEmployment ? new Date(job.dateOfEmployment).toLocaleDateString() : 'Desconocido';
        jobInfo.innerHTML = `
            <p>Empleador: ${job.employerName}</p>
            <p>Título: ${job.employeeTitle}</p>
            <p>Fecha de Empleo: ${dateOfEmployment}</p>
            <p>Actual: ${job.currentEmployer ? 'Sí' : 'No'}</p>
        `;
        column.appendChild(jobInfo);
        row.appendChild(column);
    });

    employmentSection.appendChild(row);
    document.querySelector('.container').appendChild(employmentSection);
}

// Suponiendo que 'data' es tu JSON parseado
displayBasicInfo(jsonData[0].subject);
displayAccountInfo(jsonData[0].revolvingAccounts);
displayEmploymentHistory(jsonData[0].subject.employmentHistory);
