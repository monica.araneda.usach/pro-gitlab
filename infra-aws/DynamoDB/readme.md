 

## Instalación

Sigue estos pasos para crear Base de Datos:

```bash
sam package --template-file templateDynamoDB.yml --output-template-file packaged.yaml --s3-bucket aws-sam-cli-managed-default-samclisourcebucket-gerr7atye04x

sam deploy --template-file packaged.yaml --stack-name auth4 --capabilities CAPABILITY_IAM


aws dynamodb create-table \
    --table-name Tenant \
    --attribute-definitions \
        AttributeName=tenantID,AttributeType=S \
    --key-schema \
        AttributeName=tenantID,KeyType=HASH \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb put-item \
    --table-name Tenant \
    --item '{
        "tenantID": {"S": "QYGOIqsYE8"},
        "organization": {"S": "Eserp Agency"},
        "fecha_consumo": {"S": "2024-04-01T12:00:00Z"}
    }'

aws dynamodb put-item \
    --table-name Tenant \
    --item '{
        "tenantID": {"S": "s7DxkB9vr2"},
        "organization": {"S": "Cia 2"},
        "fecha_consumo": {"S": "2024-05-01T12:00:01Z"}
    }'

aws dynamodb put-item \
    --table-name Tenant \
    --item '{
        "tenantID": {"S": "dv9uydOCZi"},
        "organization": {"S": "Cia 2"},
        "fecha_consumo": {"S": "2024-05-01T12:00:03Z"}
    }'

aws dynamodb create-table \
    --table-name Clientes \
    --attribute-definitions \
        AttributeName=id_trx,AttributeType=S \
    --key-schema \
        AttributeName=id_trx,KeyType=HASH \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5


aws dynamodb put-item \
    --table-name Clientes \
    --item '{
        "id_trx": {"S": "uuid1"},
        "cliente_id": {"N": "101"},
        "servicio_consumido": {"S": "Servicio A"},
        "fecha_consumo": {"S": "2024-04-01T12:00:00Z"},
        "token": {"S": "token123"}
    }'

aws dynamodb put-item \
    --table-name Clientes \
    --item '{
        "id_trx": {"S": "uuid2"},
        "cliente_id": {"N": "102"},
        "servicio_consumido": {"S": "Servicio B"},
        "fecha_consumo": {"S": "2024-04-02T12:00:00Z"},
        "token": {"S": "token456"}
    }'

aws dynamodb put-item \
    --table-name Clientes \
    --item '{
        "id_trx": {"S": "uuid3"},
        "cliente_id": {"N": "103"},
        "servicio_consumido": {"S": "Servicio C"},
        "fecha_consumo": {"S": "2024-04-03T12:00:00Z"},
        "token": {"S": "token789"}
    }'

aws dynamodb describe-table --table-name Clientes


aws dynamodb query \
    --table-name Clientes \
    --key-condition-expression "id_trx = :idTrx" \
    --expression-attribute-values  '{":idTrx":{"S":"uuid1"}}'

 aws dynamodb delete-table --table-name Tenant

aws dynamodb create-table \
    --table-name TrxMultitenant \
    --attribute-definitions \
        AttributeName=id_transaccion,AttributeType=S \
        AttributeName=fecha,AttributeType=S \
    --key-schema \
        AttributeName=id_transaccion,KeyType=HASH \
        AttributeName=fecha,KeyType=RANGE \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5
    

################################    5 may 2024


aws dynamodb put-item --table-name Cliente --item '{
    "id": {"S": "cliente1"},
    "nombre": {"S": "John Doe"},
    "apellido": {"S": "Doe"},
    "email": {"S": "johndoe@example.com"},
    "fono": {"S": "1234567890"},
    "fecha_creacion": {"S": "2023-01-01"},
    "fecha_actualizacion": {"S": "2023-01-02"}
}'

aws dynamodb put-item --table-name Producto --item '{
    "id": {"S": "producto1"},
    "nombre": {"S": "Producto Uno"},
    "descripcion": {"S": "Descripción del Producto Uno"},
    "categoria": {"S": "Categoría A"},
    "fecha_creacion": {"S": "2023-01-01"},
    "fecha_actualizacion": {"S": "2023-01-02"}
}'

aws dynamodb put-item --table-name Servicio --item '{
    "id": {"S": "servicio1"},
    "nombre": {"S": "Servicio Uno"},
    "descripcion": {"S": "Descripción del Servicio Uno"},
    "fecha_creacion": {"S": "2023-01-01"},
    "fecha_actualizacion": {"S": "2023-01-02"}
}'


aws dynamodb put-item --table-name Tarifa --item '{
    "id": {"S": "tarifa1"},
    "servicio_id": {"S": "servicio1"},
    "descripcion": {"S": "Tarifa básica"},
    "monto": {"N": "100.00"},
    "moneda": {"S": "USD"},
    "periodo": {"S": "Mensual"},
    "fecha_inicio": {"S": "2023-01-01"},
    "fecha_fin": {"S": "2024-01-01"}
}'


aws dynamodb put-item --table-name Operacion --item '{
    "id": {"S": "operacion1"},
    "id_movimiento": {"S": "movimiento1"},
    "descripcion": {"S": "Operación inicial"},
    "estado": {"S": "completado"},
    "token_autorizacion": {"S": "tok123"}
}'

aws dynamodb put-item --table-name TrxMovimientos --item '{
    "id": {"S": "movimiento1"},
    "tenant_id": {"S": "tenant1"},
    "producto_id": {"S": "producto1"},
    "servicio_id": {"S": "servicio1"},
    "fecha": {"S": "2023-01-01"},
    "estado": {"S": "activo"},
    "tipo": {"S": "tipo1"},
    "monto": {"N": "200.00"}
}'

aws dynamodb put-item --table-name ProductoServicio --item '{
    "producto_id": {"S": "producto1"},
    "servicio_id": {"S": "servicio1"}
}'

aws dynamodb put-item --table-name Tenant --item '{
    "id": {"S": "tenant1"},
    "nombre": {"S": "Empresa Uno"},
    "descripcion": {"S": "Descripción de Empresa Uno"},
    "fecha_creacion": {"S": "2023-01-01"},
    "fecha_actualizacion": {"S": "2023-01-02"},
    "condiciones": {"S": "Condiciones generales"}
}'

aws dynamodb put-item --table-name Tenant --item '{
    "id": {"S": "tenant2"},
    "nombre": {"S": "Empresa Dos"},
    "descripcion": {"S": "Descripción de Empresa Dos"},
    "fecha_creacion": {"S": "2023-02-01"},
    "fecha_actualizacion": {"S": "2023-02-02"},
    "condiciones": {"S": "Condiciones específicas"}
}'

aws dynamodb put-item --table-name Tenant --item '{
    "id": {"S": "tenant3"},
    "nombre": {"S": "Empresa Tres"},
    "descripcion": {"S": "Descripción de Empresa Tres"},
    "fecha_creacion": {"S": "2023-03-01"},
    "fecha_actualizacion": {"S": "2023-03-02"},
    "condiciones": {"S": "Sin condiciones especiales"}
}'