import random
import string

def generar_token(largo=10):
    """Genera un token alfanumérico aleatorio."""
    caracteres = string.ascii_letters + string.digits  # Incluye letras y números
    token = ''.join(random.choice(caracteres) for _ in range(largo))
    return token

# Generar 3 tokens
tokens = [generar_token() for _ in range(3)]
print(tokens)
