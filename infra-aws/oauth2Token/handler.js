const axios = require('axios');
const qs = require('querystring');

exports.oauth2Token = async (event) => {
    const endpoint = 'https://api.sandbox.equifax.com/personal/consumer-data-suite/oauth/token';

    // Leer los datos enviados a través del cuerpo del evento, que es procesado por API Gateway
    const { apiKey, clientAssertion, scope = 'delivery', grantType = 'jwt-bearer' } = JSON.parse(event.body);
 

    try {
        const response = await axios.post(endpoint, qs.stringify({
            grant_type: grantType,
            api_key: apiKey,
            client_assertion: clientAssertion,
            scope: scope
        }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        return {
            statusCode: 200,
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({  
                access_token: response.data.access_token,
                token_type: response.data.token_type,
                refresh_token: response.data.refresh_token,
                expires_in: response.data.expires_in,
                scope: response.data.scope
            })
        };

    } catch (error) {
        console.error('Error al generar el oAUTH2 Token:', error);
        return {
          statusCode: error.response ? error.response.status : 500,
          body: JSON.stringify({ message: error.message })
        };
    }
};
