# OAuth2 Token

Este código proporciona una función llamada `oauth2Token` que se utiliza para obtener un token OAuth2 a través de la API de Equifax Sandbox. La función está diseñada para ser utilizada dentro de un entorno de servidor sin estado, como AWS Lambda.

## Requisitos

- Node.js instalado en tu sistema.
- Axios, una biblioteca para realizar solicitudes HTTP desde Node.js. Puedes instalarlo ejecutando `npm install axios`.
- qs, una biblioteca para serializar y analizar cadenas de consulta en formato URL. Puedes instalarlo ejecutando `npm install querystring`.

## Uso

1. Copia el código proporcionado en un archivo JavaScript, como `oauth2Token.js`.
2. Asegúrate de tener una cuenta en la API de Equifax Sandbox y obtén las claves de API necesarias.
3. Asegúrate de que los datos requeridos se envíen correctamente al cuerpo del evento.
4. Ejecuta la función según sea necesario.

## Configuración

El código utiliza Axios para realizar una solicitud POST a la API de Equifax Sandbox para obtener un token OAuth2. Asegúrate de proporcionar las claves de API y el alcance adecuados en el cuerpo de la solicitud.

## Manejo de Errores

El código maneja los errores de manera adecuada. Si la solicitud falla, devolverá un objeto JSON con un mensaje de error y un código de estado correspondiente.

## Contribución

Siéntete libre de contribuir con mejoras a este código a través de solicitudes de extracción. ¡Toda contribución es bienvenida!

## Licencia

Este código se proporciona bajo la [Licencia MIT](LICENSE).
