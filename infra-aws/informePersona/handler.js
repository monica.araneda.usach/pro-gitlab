const axios = require('axios');

exports.informePersona = async (event) => {

/*  Extraer el token de autorización del cuerpo del evento o del parámetro de consulta
    Asumiendo que el token viene en el cuerpo del evento como un campo 'token'
*/    


const requestBody = JSON.parse(event.body);
const token = requestBody.token;
console.log(`Received token: ${token}`);


let config = {
    method: 'get',
    maxBodyLength: Infinity,
    url: 'https://api.sandbox.equifax.com/personal/consumer-data-suite/v1/creditReport/bahdguy-efx-001/summary',
    headers: { 
        'Authorization': `Bearer ${token}`
    } 
};

try {
    const response = await axios.request(config);
    const data = response.data; 
    
    return {
    statusCode: 200,
    body: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json'
    }
    };
} catch (error) {
    console.error('Error al informePersona:', error);
    return {
        statusCode: error.response ? error.response.status : 500,
        body: JSON.stringify({
            message: error.message,
            responseData: error.response ? error.response.data : null // Include server response to help diagnose the issue.
        }),
        headers: {
            "Content-Type": "application/json"
        }
    };
}

};