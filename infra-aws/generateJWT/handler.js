const axios = require('axios');
const qs = require('querystring');
const AWS = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');

AWS.config.update({region: 'us-east-1'}); 
const dynamoDb = new AWS.DynamoDB.DocumentClient();

exports.generateJWT = async (event) => {
    const endpoint = 'https://api.sandbox.equifax.com/personal/consumer-data-suite/oauth/generate-jwt-claim';
    const symmetricKey = '9qPQUcx3opE6LzwWXISka0WyUR4QXp0Y541T1yhm5rE';
    const base64PrivateKey = 'MIIJKQIBAAKCAgEA0apwNolXYFgVb1C6uJlPP2zI0EgCJP9VMqynYyLDFyDqCd0YMBS7SyGfIO9YuiahUgEPuO8bAjMNUwwsOLcbHdfflglNZTc72YAgrNS9pSbdoDHHFib1jCQA7CkV32P6+oRZ/c0lqEjHdXedBj6r4jYCeBxwWnPj4HIpbYS4NPsezcIZHi93SJndS0bhMsgnfS3nrX3M5hOeVhKN7VHBJWWguVi5hDYrgxRLpYMmkbji9aTZ4dlfAuqFC3sWYH3DkCM9sU0YZ+6Gwmmf7aqAZTzlSy4rJNlSe1JNdipWOZUOTKsG5nWPE0kwZW2KL9qcgtY+0VuD0tgVNzCKPTJ/1TiiSnBMfaMUBBjtk0YCU+mnwpJ1mu2p4Qbiwfa7pThdpv5xDODABHssB0CKdfk3fd9Sno9fF7UuIxd9fpVl71GRmlXmxkqhi0hENM7/LHYAcRkRoPy6hbx7YifJwxaC6nWbU5ciq2HrUhep5mfMiMR6wsNMFlKt7v6yCeBAhXZ3EJQTQN8z76v3GKrk0QIVtMmBkQ/YLEx9tfmvLTWzaQ0Phy0YourY13ZWRoXLsMyGgGLP42uTLQ4OZwdgKxUiAMH9FBea6pQc+/07Z0pHcMNnD2qSCdH3Po9mFRhKR2ijqaeZk75jTLtDJRFGEfidRlvgEfkYrMjuSTtRj56YR1MCAwEAAQKCAgEAsgqtp2IRVLr32MMRDFkDtrRGArhjOc+pJZL/wWtvh2Bn3WBmfc3dDfvLCUi2a70KGHzWQRCqOr/nfLJFixPSbxE1yE617lFs/VGUba9qK6hJYxNiICmNMigQsHpjHIPmSH3DfYaLh8/lsJaWPQtTIpECn8RmlCj9RUcsbtfZJQRqMAe3FK7x+DDQfm/+JAVboy0sI2NEb8wRFL5Ez1YxUfRu+3Wgc+kcIf9GDZLFWdFKyZXnf6X6zJshx5IKen3RZ8DFTp5NL1SaEtVm/Ydf6JhBjgnykSQKqrZFGSbyeYtdzTUecu05RgNMmZTfV6uoZGJSja3Mz2q2+Vq6ySTs7zRltsQ6leHCPpAB586MvnaeUYjBkG2tNWhpVCAkZE2P7F/QU5Cwv5y9/7UCKPQ+ezk8V1Sdram4y0QE8mcxke9Kc4WU02Vs+1rGxRxgu9ISPOM/8CnZfB8mDEOnph53gu01A6RQ2Uqe1+WiI/DKgBJo+c0Bq0KXALGNRBIgwlY9ra1FPgglhh5PbKspWsuGDbGumv6Wny63Bij2Dr0GB7pkSomrreOP/8IjM8BiioaMGP+mNKmoiVjCOuKUI6eUMmh5SCEbUr2tBjrI4oeB7c2H/kTVfUrxPjkF9V9TLp/xoqY4h59sSFwWuXq/QgH2HK95n5nsYqCM8oea7iJjHEECggEBAPy+wkrZ0/0EwveLyde5H6BJYbiy9QVW8MZC7Ery8PuxrzYduCNiIk/HFlyVaOZRweN520KgsS9S+wUS9ozamrRDmmZ+4ilO1h6rzDsgtvGKdlBZkypDM7Nq6PwFYdcOTU5j7jGAPdk1mXpw23nqKvjF9737BIJgkl9AsYLTu3iW2zm7aVy8jUGtNgPdlGxxeP4h0dyAcs53HA2cowOSvUQAZMN+1KAyfpNznZVFGuNztbzG5CtUnL/sW6FKS/6W7vb5h4wtGIqf6lNUddpFRsCf3zZXsJxnCvYDFfe927DvBk6LzZ5P6oTdiwWSDQO3Ng+aBDvUzd+G3FkoEQVOnOkCggEBANRdqCdIHK8qwtJxjnmUwA5t/9Afc9gt+ht/tKeRaOqkrC2Kf0tbyNNl1yv3amgAETbSJ5gWXFT+HLJsDCUoDpkps69oqNEBTV5uN7q4rs1jo7n+v+0s5swFeQjJ+9XeVY4lhpWoCFFX7KpsuKF1I8H8ddZ4+mOjsruH10qUWNYAuMCnBT7r+aVh66gWDt/v45X1D2tYUtnCIXBLUuOz+nsBiFJHGX8B9tdHnMK3qd6XefvgBMo9QwmTiaf/udRJCnwwf2CrdhsUgHz0cVWjmaV/+yJq857a6zioIgtngFRmOTeNE1BSM9MBBRJz34/mdM9mdd4JIp6gzQojgapgLNsCggEAT4tx9sDLuSwQeHxkUCKRU48E1G5uMvoD2UIiwAvp0C1B6ZQ86dXu2pDb9tOHKkW88ezeFwAgA7boDdbEBdyfJ/DG3U89uqZ3OTv0kP9g1HAxgGRQrtJrqSISpY7EFj0IE43b33RvAred0C1Ixfvu/lDAAejIqIfgSV5A/93HeNJxuJ/FL0ygYHU1NM498ktcOw9Fz1IVz+koyRftLii2rHo7PFPkc26w65NNmzcemUtHfVxTX1fH5Zdr1z1c4Yl7N/ucr39Z4/ckdol0dferpuAqDh5XFUJQ7tBVz5zPUcZIg9v4IMUiGCLpqB0pP2wf9ZURlAsLqDf3jY/Qm4z8UQKCAQEAu3DS+6D9mGqgvMh4Hb36aoISmas5P6UNLXphhMmmlfy08BlfU692qYgpDGh75cGxb/a79dQ+uNH1mFIfwigCApQ5QeiZoKiKln+n9soNl8GLgJ5m3ruUFp35/5TwEAwG7ckXaj5+4PU/Mgl2SSQwZIclpyDuuEWDe1xh3TTk2DaY24pP6gK0PZIZQyn9BREiinhbnL6/me5sP0ZsqghwXGO05BJXYa/lJIzTw1YCvDJs+7D16V97LRepF7xyU/oIJCdWNogAXdSRuDi1VAZrlhVJH07RKvRFS6OzMOew8E2t0OdXFb0IewRaargVqliZ33mrLUhY4zRA5eMDDtOGowKCAQBUdiZqxIV97lrjCygiBZBzTRuuNI8NOaYFV0sblBSwl8ijJpkbo4tEhnhhkrMz01uMlcfkkFjZO7XsmlidjoVdV1iQtvB8iXybUCz2TrBo6+pXg0jydXa7k2o/AmeWzPktH9Ladi07rM3FW/IUPT0PtHm6DGKKmZrqZgM3c8/GSTlVS7EmdIocmTo8ojQ06QhXvOX0Az3BsAkLIWbhM5f9D294eNwA31weT4III61QBrNS0c+B2aNboITNg8ICqH2gzHRODM/JAGCkVNHXEq30cI3lxStl49JePfkEJGpxBjzFauQIvTRSycpinX6MlGET7jSL2eSTrS7eRqVW4btC'; // Truncated for brevity
    const scope = 'delivery';
    const issuer = 'EFX';
    const subject = 'mock_delivery:bahdguy';
    const expiration = '59';
    try {
        
        const requestBody = JSON.parse(event.body);
        const tenantId = requestBody.tenantId;
        console.log(`Received tenantId: ${tenantId}`);

        
       // Intenta obtener el ítem de la tabla DynamoDB
        const params = {
            TableName: 'Tenant',  // Reemplaza esto con el nombre real de tu tabla
            Key: {
                'tenantID': `${tenantId}`
            }
        };
        console.log( `>>>>> param  ${params} `);
         const  data = await dynamoDb.get(params).promise();
         if (!data || !data.Item) {
            return {
                statusCode: 404,
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    "error": "Cliente no encontrado",
                    "mensaje": "El cliente con el ID especificado no existe en nuestra base de datos. Por favor, verifica la información proporcionada e inténtalo de nuevo."
                })
            };
        } 
        
        
        // Generar un UUID
        insertTransaction();
        
        const response = await axios.post(endpoint, qs.stringify({
        symmetricKey: symmetricKey,
        base64PrivateKey: base64PrivateKey,
        scope: scope,
        issuer: issuer,
        subject: subject,
        expiration: expiration
        }), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

      return {
                statusCode: 200,
                headers: {
                "Content-Type": "application/json"
                },
                body: JSON.stringify({ jwe: response.data.jwe })
                };

    } catch (error) {
        console.error('Error al generar el JWT:', error);
    }
};

async function insertTransaction() {
    const id_transaccion = uuidv4();
    const fecha = new Date().toISOString();
    const params = {
        TableName: 'TrxMultitenant',
        Item: {
            id_transaccion: id_transaccion,
            fecha: fecha,
            tenantId: 'tenant123',
            servicio: 'id1',
            tipo_transaccion: 'consulta',
            estado: 'exitoso',
            detalles: {
                tiempo_respuesta_ms: 50,
                codigo_respuesta: 200,
                mensaje_respuesta: 'OK'
            }
        }
    };

    try {
        await dynamoDb.put(params).promise();
        console.log('Transacción insertada correctamente:', params.Item);
    } catch (error) {
        console.error('Error al insertar la transacción:', error);
    }
}