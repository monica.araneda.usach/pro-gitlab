# Generador de JWT para Equifax API

Este repositorio contiene una función de AWS Lambda implementada en Node.js que se encarga de generar un token JWT utilizando la API de Equifax. El token generado permite realizar solicitudes autenticadas a otros servicios que requieran autenticación basada en JWT.

## Descripción General

La función `generateJWT` hace una solicitud POST a un endpoint de Equifax para obtener un JWT. Utiliza una clave simétrica y una clave privada codificada en base64 para la generación del token.

## Componentes Clave

- `axios`: Utilizado para hacer solicitudes HTTP.
- `qs` (querystring): Utilizado para formatear los parámetros de la solicitud HTTP.
- `generateJWT`: Es la función principal que se invoca al ejecutar la lambda, la cual realiza la solicitud para generar el JWT.

## Cómo Funciona

La función `generateJWT` construye los datos necesarios y realiza una solicitud POST al endpoint de Equifax. Los datos incluyen:
- `symmetricKey`: Clave simétrica para el cifrado.
- `base64PrivateKey`: Clave privada en formato base64.
- `scope`, `issuer`, `subject`, `expiration`: Parámetros adicionales requeridos por Equifax para generar el JWT.

Si la solicitud es exitosa, la función retorna el JWT en el cuerpo de la respuesta. En caso de error, se registra en la consola.

## Configuración

### Requisitos

- Node.js instalado localmente o en el entorno donde se desplegará la función Lambda.
- Dependencias de Node.js: `axios` y `querystring`. Estas se pueden instalar mediante npm:
  ```bash
     npm install axios querystring
    ```

## Despliegue

### Crear una función Lambda en AWS

1. **Accede a AWS Management Console**.
2. **Dirígete a AWS Lambda** y crea una nueva función.
3. **Selecciona Node.js** como entorno de ejecución.
4. **Sube el código proporcionado** o pégalo en el editor en línea de la consola de Lambda.

### Configuración de permisos y variables de entorno

- **Configura las variables de entorno** para las claves y demás parámetros sensibles.
- **Configura los permisos de IAM** para que la función Lambda pueda interactuar con la API de Equifax.

### Prueba y monitoreo

- **Utiliza las herramientas de monitoreo y logs de AWS Lambda** para verificar el correcto funcionamiento de la función.
- **Prueba la función mediante la consola de AWS** o invocándola desde un cliente HTTP como Postman.

## Ejemplo de Uso

```javascript
const lambdaResponse = await lambda.invoke({
  FunctionName: 'NombreDeTuFuncionLambda',
  Payload: JSON.stringify(eventoDePrueba)
});
console.log('Respuesta:', JSON.parse(lambdaResponse.Payload));
```
