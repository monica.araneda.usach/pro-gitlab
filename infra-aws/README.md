# Proyecto SAM - INFRA CreditScore

Servicio Backend Lambda - API

## Índice

- [LAMBDAS](#Lambdas)
- [APIS](#apis) 

## Lambdas

Explica en más detalle qué hace tu proyecto y por qué es útil. Añade algunas capturas de pantalla si es aplicable.

## APIs

Lista las tecnologías, librerías y frameworks que usas en el proyecto.

- Tech 1
- Tech 2
- Tech 3

 

## Instalación

Sigue estos pasos para sincronizar Recursos:

```bash
# Compilamos los cambios de Lambda JWT
sam  build -t templateJWT.yml

# Sincronizamos el Lambda JWT con el zip handler de la nueva compilacion
sam deploy --guided
 